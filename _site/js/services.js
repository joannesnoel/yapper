angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://pbs.twimg.com/profile_images/479740132258361344/KaYdH9hE.jpeg'
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Did you get the ice cream?',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  // Some fake testing data
  var friends = [{
    id: 0,
    name: 'Ben Sparrow',
    notes: 'Enjoys drawing things',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    notes: 'Odd obsession with everything',
    face: 'https://pbs.twimg.com/profile_images/479740132258361344/KaYdH9hE.jpeg'
  }, {
    id: 2,
    name: 'Andrew Jostlen',
    notes: 'Wears a sweet leather Jacket. I\'m a bit jealous',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    notes: 'I think he needs to buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    notes: 'Just the nicest guy',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];


  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  }
})

.factory('Profile', function($http) {
  return {
    get: function(userId) {
      //return friends[friendId];
      var fd = new FormData();
      fd.append('_id', userId);
      return $http.post('http://109.74.198.86:3000/profile', fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      });
    }
  }
})


.factory('MyAccount', function(){
  return {
    '_id'           : null,
    'birthdate'     : '',
    'fullname'      : '',
    'email'         : '',
    'username'      : '',
    'gender'        : 'male',
    'photos'        : null,
    'profile_photo' : 'img/add_photo.png',
    'website'       : '',
    'bio'           : '',
    'phone'         : ''
  }
})

.factory('Chat', function(){
  return {
    messages: []
  };
})

.factory('SocketFunc', function(MyAccount, Chat, $ionicScrollDelegate, $ionicLoading, $localstorage, $http, $state){
  var functions = {
    //socket.on('broadcast received'_
    broadcastReceived: function(data){
      // $('ion-view.chatroom-title ion-content .scroll').append('<div class="chat"> \
      //                               <img src="' + data.user.profile_photo + '" /> \
      //                               <div>' + data.message + '</div> \
      //                               <div style="clear:both"></div> \
      //                             </div>');
      if(data._user._id == MyAccount._id)
        data.from_me = 'me';
      else
        data.from_me = '';
      Chat.messages.push(data);
      $ionicScrollDelegate.resize();
      $ionicScrollDelegate.scrollBottom();
    },
    //socket.on('join room success',
    joinRoomSuccess: function(data){
      MyAccount.room = data.room;
      $localstorage.setObject('MyAccount',MyAccount);
      
      while(Chat.messages.length > 0)
        Chat.messages.pop();

      $ionicLoading.show({
        content: 'Loading messages.',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0
      });

      var fd = new FormData();
      fd.append('room', $localstorage.get('location'));
      $http.post('http://109.74.198.86:3000/chat_messages', fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      }).success(function(chats){
        $ionicScrollDelegate.scrollBottom();
        $ionicLoading.hide();

        for(var i=chats.length-1; i>=0; i--){
          if(chats[i]._user._id == MyAccount._id)
            chats[i].from_me = 'me';
          else
            chats[i].from_me = '';
          Chat.messages.push(chats[i]);
       }
      });

    },
    userNotFound: function(data){
      $localstorage.unset('MyAccount');
      $state.go('home');
    },
    roomName: 'Yapper'
  }
  return functions;
})

.factory('AccountFunc', function(MyAccount, $ionicActionSheet, $jrCrop, $http, $localstorage,$state){
  var functions = {
    register: function(){
      
      var fd = new FormData();
      fd.append('email', MyAccount.email);
      fd.append('username', MyAccount.username);
      fd.append('password', MyAccount.password);
      if(MyAccount.profile_photo != 'img/add_photo.png'){
        var binary = atob(MyAccount.profile_photo.split(',')[1]);
        var mimeString = MyAccount.profile_photo.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for(var i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        file = new Blob([new Uint8Array(array)], {type: mimeString});
        var ext;
        switch(mimeString){
          case 'image/jpeg' : ext = '.jpg'; break;
          case 'image/gif'  : ext = '.gif'; break;
          case 'image/png'  : ext = '.png'; break;
          default: ext = '.dat';
        }
        fd.append('photo', file, 'temp'+ext);
      }

      $http.post('http://109.74.198.86:3000/register', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(response){
          delete response.userdata.__v;
          //MyAccount = response.userdata;
          MyAccount.profile_photo = response.userdata.profile_photo;
          MyAccount.email =  response.userdata.email;
          MyAccount.username =  response.userdata.username;
          MyAccount._id =  response.userdata._id;

          $localstorage.setObject('MyAccount',MyAccount);

          socket.emit('init', {
              _user: MyAccount._id,
              room: $localstorage.get('location')
          });

          $state.go('tab.account');
        });
    },
    updateInfo: function(field){
      var fd = new FormData();
      fd.append('field', field);
      fd.append('value', MyAccount[field]);
      fd.append('_id', MyAccount._id);
      $http.post('http://109.74.198.86:3000/update', fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(response){
          var fieldname = field;
          MyAccount[fieldname] = response;
          $localstorage.setObject('MyAccount',MyAccount);
        });
    },
    checkEmail: function(){
      console.log('checking email...');
      var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      var valid = pattern.test(MyAccount.email);
      if(valid){
        MyAccount.username = MyAccount.email.match(/^([^@]*)@/)[1];
        functions.checkUsername();
      }
    },
    checkUsername: function(){
      console.log('checking username...');
    },
    checkPassword: function(){
      console.log('checking password...')
    },
    addPhoto: function(){
      $ionicActionSheet.show({
        buttons: [
          { text: 'Take Photo' },
          { text: 'Choose from Library' }
        ],
        titleText: 'Profile Picture',
        cancelText: 'Cancel',
        cancel: function() {
              // add cancel code..
            },
        buttonClicked: function(index) {
            if(index == 0)
              var sourceType = Camera.PictureSourceType.CAMERA;
            else
              var sourceType = Camera.PictureSourceType.PHOTOLIBRARY;

              navigator.camera.getPicture(function(url){
              
              $jrCrop.crop({
                  url: url,
                  width: 200,
                  height: 200,
                  //title: 'Move and Scale'
              }).then(function(canvas) {
                  var image = canvas.toDataURL();
                  MyAccount.profile_photo = image;
                  $rootScope.$apply();
              }, function(err) {
                  console.log(err);
              });
            }, function() {
                // handle error
            },
            {  //camera options
              targetWidth: 800,
              targetHeight: 800,
              quality: 50,
              destinationType: Camera.DestinationType.FILE_URI,
              sourceType: sourceType,
              cameraDirection: Camera.Direction.FRONT
            });
          return true;
        }
      });
    }
  }
  return functions;
})

.factory('$localstorage', ['$window', function($window) {
  return {
    unset: function(key){
      delete $window.localStorage[key];
    },
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);
