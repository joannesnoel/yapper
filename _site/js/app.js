// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform, $rootScope, $localstorage, $http, $location, $ionicPopup, MyAccount, SocketFunc) {
  $ionicPlatform.ready(function() {
    cordova.plugins.Keyboard.disableScroll(true);
    socket = io.connect('http://109.74.198.86:8111');
    socket.on('connect', function(){

      function getLocation(){
        navigator.geolocation.getCurrentPosition(function(geo){
          $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+geo.coords.latitude+','+geo.coords.longitude+'&sensor=true')
          .success(function(response){
            if(response.status == 'OK'){
              for(var i=0; i<response.results.length; i++){
                if(response.results[i].types.indexOf('locality') >= 0){
                  $localstorage.set('location',response.results[i].formatted_address);
                  SocketFunc.roomName = response.results[i].address_components[0].short_name;
                  break;
                }
              }

              if($.isEmptyObject($localstorage.getObject('MyAccount')) == false){
                me = $localstorage.getObject('MyAccount');

                MyAccount._id = me._id;
                MyAccount.birthdate = me.birthdate;
                MyAccount.fullname = me.fullname;
                MyAccount.email = me.email;
                MyAccount.gender = me.gender;
                MyAccount.profile_photo = me.profile_photo;
                MyAccount.username = me.username;

                socket.emit('init', {
                    _user: MyAccount._id,
                    room: $localstorage.get('location')
                });

                $location.path('tab/dash');
              }

            }else
              console.log(response);
          });
        }, function(err){
          var alertPopup = $ionicPopup.alert({
            title: 'Please turn on Location Services',
            template: '<p>Yapper connects you to people near you. You need to turn on location services to proceed.</p>\
            <p>Go to Settings > Privacy > Location Services (enabled) > Yapper and then select "While Using the App"</p>'
          },{
            text: 'Continue',
          });
           alertPopup.then(function(res) {
            getLocation();
          });
        });
      }
      getLocation();

    });

    socket.on('join room success', SocketFunc.joinRoomSuccess);
    socket.on('broadcast received', SocketFunc.broadcastReceived);
    socket.on('user not found', SocketFunc.userNotFound);

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
    document.addEventListener("resume", function(){
      // if(socket.disconnected)
      //   socket = io.connect('http://109.74.198.86:8111',function (response) {
      //     console.log(response);
      //   });
    }, false);
    
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.backButton.previousTitleText(false).text('').icon('ion-chevron-left');
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.profile', {
    url: '/profile/:userId',
    views: {
      'tab-dash': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })
    .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
    })
    
  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('home', {
    url: '/home',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'SignupCtrl'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home');

});