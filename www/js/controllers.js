angular.module('starter.controllers', [])

.controller('DashCtrl', function(SocketFunc, MyAccount, Chat, $scope, $localstorage, $http) {
  $scope.chats = Chat.messages;
  $scope.title = SocketFunc.roomName;
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  $scope.send = function(){
    $scope.sending = true;
    socket.emit('broadcast to room',{
      _user: MyAccount._id,
      room: $localstorage.get('location'),
      message: $scope.message 
    });
    socket.on('broadcast success', function(data){
      $scope.sending = false;
      $scope.message = "";
    })
  }

  $scope.moreMessages = function() {
    var fd = new FormData();
      fd.append('room', $localstorage.get('location'));
      fd.append('before_msg_id', Chat.messages[0]._id);
      $http.post('http://109.74.198.86:3000/chat_messages', fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
      }).success(function(chats){
        for(var i=0; i<chats.length; i++){
          if(chats[i]._user._id == MyAccount._id)
            chats[i].from_me = 'me';
          else
            chats[i].from_me = '';
          Chat.messages.unshift(chats[i]);
        }
        $scope.$broadcast('scroll.refreshComplete');
        if(!$scope.$$phase) {
          $scope.$apply()
        }
      });
  };

})

.controller('ChatsCtrl', function($scope, Chats) {
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('FriendsCtrl', function($scope, Friends) {
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  $scope.friends = Friends.all();
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  $scope.friend = Friends.get($stateParams.friendId);
})

.controller('ProfileCtrl', function($scope, $stateParams, $http, Profile, MyAccount) {
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  if($stateParams.userId == MyAccount._id)
    $scope.user = MyAccount;
  else{
    Profile.get($stateParams.userId).success(function(data){
      $scope.user = data;
    });
  }
})

.controller('AccountCtrl', function(MyAccount, AccountFunc, $scope) {
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);

  $scope.MyAccount = MyAccount;
  $scope.addPhoto = AccountFunc.addPhoto;
  $scope.updateInfo = AccountFunc.updateInfo;

  // var dt = new Date('13 June 2013');
  // dt.setYear(dt.getYear()-16)
  // var options = {
  //   date: dt,
  //   mode: 'date',
  //   maxDate: dt
  // };

  // datePicker.show(options, function(date){
  //   alert("date result " + date);  
  // }); 
})

.controller('HomeCtrl', function(MyAccount, $scope,$ionicNavBarDelegate, $localstorage){
  if(window.cordova.platformId != "browser")
    cordova.plugins.Keyboard.disableScroll(false);
  
  $scope.MyAccount = MyAccount;
  $scope.normal_signup = false;
  $('#signup .email').keydown(function(e){
    if($(this).val().length <= 1 && e.keyCode == 8){
      $(this).css({'width':'142px','text-align':'left'});
      $scope.normal_signup = false;
    }else if($(this).val() == "" || e.keyCode != 8){
      $(this).css({'width':'100%','text-align':'center'});
      $scope.normal_signup = true;
    }
  });
})

.controller('SignupCtrl', function(MyAccount, $scope, AccountFunc){
  cordova.plugins.Keyboard.disableScroll(true);
  $scope.MyAccount = MyAccount;
  $scope.addPhoto = AccountFunc.addPhoto;
  $scope.check_email = AccountFunc.checkEmail;
  $scope.check_username = AccountFunc.checkUsername;
  $scope.check_password = AccountFunc.checkPassword;
  $scope.register = AccountFunc.register;
});
